# Erfa-Karte für ccc.de/regional

## Schriftart

Die im Repository enthaltene Schriftart wurde von Johan Kallas (johankallas@gmail.com) erstellt und ist unter der SIL Open Font License v1.10 lizensiert.